/*
 * The simple cd command implemention used by fstk.
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * can be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include "fstk.h"
#include "file.h"
#include "dirent.h"
#include "fstk_malloc.h"

void fstk_cd(struct fstk *fs, char *dst_dir)
{
        DIR *old = this_dir;

        if (*dst_dir) {
                this_dir = fstk_opendir(fs, dst_dir);
		if (!this_dir) {
			printf("cd: %s: no such directory\n", dst_dir);
			this_dir = old;
			return;
		}

                if (this_dir->dd_dir->inode->mode != DIR_TYPE) {
                        printf("cd: %s: is not a directory\n", dst_dir);
                        fstk_closedir(this_dir);
                        this_dir = old;
                } else {
                        fstk_closedir(old);
                }
        }
}
