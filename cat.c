/* 
 * Simple cat implemention.
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <string.h>
#include "fstk.h"
#include "fstklib.h"
#include "file.h"
#include <unistd.h>

void fstk_cat(struct fstk *fs, int argc, char *argv[])
{
        char buf[2048] = {0,};
        int bytes_read;
        int i = 1;
        struct file *file;
        
        for (; i < argc; i++) {
                file = fstk_open(fs, argv[i]);
                if (!file) {
                        printf("cat: can't access %s: " 
                               "No such file or directory\n", argv[i]);
                        continue;
                }
                if (file->inode->mode == DIR_TYPE) {
                        printf("cat: %s: is a directory\n", argv[i]);
                        fstk_close(file);
                        continue;
                }
                
                while ((bytes_read = fstk_read(file, buf, 2048)) >= 0) {
                        write(1, buf, bytes_read);
                }
                printf("\n");                
                fstk_close(file);
        }
}        
