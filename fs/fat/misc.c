/* 
 * Some util fucntions for vfat fs
 *
 * Copyright (C) 2010 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "fstk.h"
#include "fstklib.h"
#include "cache.h"
#include "fstk_malloc.h"
#include "fat.h"


static inline int start_not_valid(int start)
{
	return start > 0x0ffffff0 || start <= 0;
}

void vfat_check_fat(struct fstk *fs)
{	
	struct fat_sb_info *sbi = FAT_SB(fs);
	struct cache_struct *cs;
	int i = 0;
	int *p;
	int *buf_end;
	int start, end, page_start, page_end;		
	int total_pages = sbi->fat_count;
	
	cs = get_fat_sector(fs, i++);
	p = (int *)cs->data;
	buf_end = (int *)((char *)p + BLOCK_SIZE(fs));
	

	while (i < total_pages) {
		start = end = *p;
		while (start_not_valid(start) && p < buf_end) {
			start = end = *++p;
		}
		/* Finally, we got a valid start */

		if (p == buf_end) {
			cs = get_fat_sector(fs, i++);
			if (i >= total_pages)
				break;
			p = (int *)cs->data;
			buf_end = (int *)((char *)p + BLOCK_SIZE(fs));
			continue;
		}
		
		/* Move on */
		page_start = i;
		while (++end == *++p) {
			if (p >= buf_end - 1) {
				cs = get_fat_sector(fs, i++);
				if (i >= total_pages)
					break;
				p = (int *)cs->data;
				buf_end = (int *)((char *)p + BLOCK_SIZE(fs));
				p--;  /* dec one */
			}
		}
		page_end = i;
		if (end - start > 1) {
			if (page_start != page_end) {
				printf("%d-%d \t\tfrom page %d to page %d\n", 
				       start, end - 1, page_start, page_end);
			} else {
				printf("%d-%d \t\tfrom page %d\n",
				       start, end - 1, page_start);
			}
		} else {
			printf("%d \t\t\tfrom page %d\n", start, page_start);
		}
	}
	
	printf("We have a total of %d pages\n", total_pages);	
}
