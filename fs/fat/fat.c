/* 
 * The vfat fs driver in fstk
 *
 * Mostly based on the vfat driver in Syslinux_project/core/fs/fat/fat.c
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "fstk.h"
#include "file.h"
#include "dirent.h"
#include "fstklib.h"
#include "cache.h"
#include "fstk_malloc.h"
#include "fat.h"


static struct fstk *this_fs = NULL;


static struct inode * new_fat_inode(void)
{
	struct inode *inode = fstk_malloc(sizeof(*inode));
	if (!inode) 
		malloc_error("inode structure");		
	memset(inode, 0, sizeof(*inode));

	/* 
	 * We just need allocate one uint32_t data to store the 
	 * first cluster number.
	 */
	inode->data = fstk_malloc(sizeof(uint32_t));
	if (!inode->data) 
		malloc_error("inode->data");

	inode->blksize = 1 << this_fs->sector_shift;

	return inode;
}

/*
 * Mangle a normal style string to DOS style string.
 */
static void mangle_dos_name(char *mangle_buf, char *src)
{       
	char *dst = mangle_buf;
	int i = 0;
	unsigned char c;        
	
	for (; i < 11; i ++)
		mangle_buf[i] = ' ';

	if ((strlen(src) == 1 && *src == '.') ||
	    (strlen(src) == 2 && *src == '.' && *(src+1) == '.')) {
		memset(dst, '.', strlen(src));
		return;
	}
	
	for (i = 0; i < 11; i++) {
		c = *src ++;
		
		if ((c <= ' ') || (c == '/')) 
			break;
		
		if (c == '.') {
			dst = &mangle_buf[8];
			i = 7;
			continue;
		}
		
		if (c >= 'a' && c <= 'z')
			c -= 32;
		if ((c == 0xe5) && (i == 11))
			c = 0x05;
		
		*dst++ = c;
	}
	mangle_buf[11] = '\0';
}


static uint32_t get_next_cluster(struct fstk *fs, uint32_t clust_num)
{
	uint32_t next_cluster;
	sector_t fat_sector;
	uint32_t offset;
	int lo, hi;
	struct cache_struct *cs;
	
	switch(FAT_SB(fs)->fat_type) {
	case FAT12:
		fat_sector = (clust_num + clust_num / 2) >> fs->sector_shift;
		cs = get_fat_sector(fs, fat_sector);
		offset = (clust_num * 3 / 2) & ((1 << fs->sector_shift) - 1);
		if (offset == 0x1ff) {
			/* 
			 * we got the end of the one fat sector, 
			 * but we don't got we have(just one byte, we need two),
			 * so store the low part, then read the next fat
			 * sector, read the high part, then combine it.
			 */
			lo = *(uint8_t *)(cs->data + offset);
			cs = get_fat_sector(fs, fat_sector + 1);
			hi = *(uint8_t *)cs->data;
			next_cluster = (hi << 8) + lo;
		} else {
			next_cluster = *(uint16_t *)(cs->data + offset);
		}
		
		if (clust_num & 0x0001)
			next_cluster >>= 4;         /* cluster number is ODD */
		else
			next_cluster &= 0x0fff;     /* cluster number is EVEN */
		if (next_cluster > 0x0ff0)
			goto fail;
		break;
		
	case FAT16:
		fat_sector = clust_num >> (fs->sector_shift - 1);
		offset = clust_num & ((1 << (fs->sector_shift-1)) -1);
		cs = get_fat_sector(fs, fat_sector);
		next_cluster = ((uint16_t *)cs->data)[offset];
		if (next_cluster > 0xfff0)
			goto fail;
		break;
		
	case FAT32:
		fat_sector = clust_num >> (fs->sector_shift - 2);
		offset = clust_num & ((1 << (fs->sector_shift-2)) -1);
		cs = get_fat_sector(fs, fat_sector);
		next_cluster = ((uint32_t *)cs->data)[offset] & 0x0fffffff;
		if (next_cluster > 0x0ffffff0)
			goto fail;
		break;
	}
	
	return next_cluster;
	
fail:  
	/* got an unexcepted cluster number, so return ZERO */
	return 0;
}


static sector_t get_next_sector(struct fstk* fs, uint32_t sector)
{
	sector_t data_area = FAT_SB(fs)->data;
	sector_t data_sector;
	uint32_t cluster;
    
	if (sector < data_area) {
		sector++;
		/* if we reached the end of root area */
		if (sector == data_area)
			sector = 0; /* return 0 */
		return sector;
	}
    
	data_sector = sector - data_area;
	if ((data_sector + 1) & FAT_SB(fs)->clust_mask)      /* in a cluster */
		return ++sector;
	
	/* get a new cluster */
	cluster = get_next_cluster(fs, (data_sector >> FAT_SB(fs)->clust_shift) + 2);
	if (!cluster ) 
		return 0;
	
	/* return the start of the new cluster */
	sector = ((cluster - 2) << FAT_SB(fs)->clust_shift) + data_area;
	return sector;
}


/* try with the biggest long name */
static char long_name[0x40 * 13];
static char entry_name[14];

static void unicode_to_ascii(char *entry_name, uint16_t *unicode_buf)
{
	int i = 0;
	
	for (; i < 13; i++) {
		if (unicode_buf[i] == 0xffff) {
			entry_name[i] = '\0';
			return;
		}
		entry_name[i] = (char)unicode_buf[i];
	}
}

/*
 * get the long entry name
 *
 */
static void long_entry_name(struct fat_long_name_entry *dir)
{
	uint16_t unicode_buf[13];
	
	memcpy(unicode_buf,      dir->name1, 5 * 2);
	memcpy(unicode_buf + 5,  dir->name2, 6 * 2);
	memcpy(unicode_buf + 11, dir->name3, 2 * 2);
	
	unicode_to_ascii(entry_name, unicode_buf);    
}


static uint8_t get_checksum(char *dir_name)
{
	int  i;
	uint8_t sum = 0;
	
	for (i = 11; i; i--)
		sum = ((sum & 1) << 7) + (sum >> 1) + *dir_name++;
	return sum;
}


/* compute the first sector number of one dir where the data stores */
static inline sector_t first_sector(struct fat_dir_entry *dir)
{
	struct fat_sb_info *sbi = FAT_SB(this_fs);
	uint32_t first_clust;
	sector_t sector;
	
	/* In case we are back from a child direnty of root by 'cd ..' */
	if (dir->first_cluster_high == 0 && dir->first_cluster_low == 0)
		return sbi->root;
	
	first_clust = (dir->first_cluster_high << 16) + dir->first_cluster_low;
	sector = ((first_clust - 2) << sbi->clust_shift) + sbi->data;
	
	return sector;
}

static inline int get_fstk_mode(uint8_t attr)
{
	if (attr == FAT_ATTR_DIRECTORY)
		return DIR_TYPE;
	else
		return FILE_TYPE;
}

static struct inode *vfat_find_entry(char *dname, struct inode *dir)
{
	struct inode *inode;
	struct fat_dir_entry *de;
	struct fat_long_name_entry *long_de;
	struct cache_struct *cs;

	char mangled_name[12] = {0, };
	sector_t dir_sector = *dir->data;

	uint8_t vfat_init, vfat_next, vfat_csum;
	uint8_t id;
	int slots;
	int entries;
	int checksum;
	int long_match = 0;

	slots = (strlen(dname) + 12) / 13 ;
	slots |= 0x40;
	vfat_init = vfat_next = slots;

	while (1) {
		cs = get_cache_block(this_fs, dir_sector);
		de = (struct fat_dir_entry *)cs->data;
		entries = 1 << (this_fs->sector_shift - 5);

		while(entries--) {
			if (de->name[0] == 0)
				return NULL;
			
			if (de->attr == 0x0f) {
				/*
				 * It's a long name entry.
				 */
				long_de = (struct fat_long_name_entry *)de;
				id = long_de->id;
				if (id != vfat_next)
					goto not_match;

				if (id & 0x40) {
					/* get the initial checksum value */
					vfat_csum = long_de->checksum;
					id &= 0x3f;

					/* ZERO the long_name buffer */
					memset(long_name, 0, sizeof long_name);
				} else {
					if (long_de->checksum != vfat_csum)
						goto not_match;
				}

				vfat_next = --id;
				
				/* got the long entry name */
				long_entry_name(long_de);
				memcpy(long_name + id * 13, entry_name, 13);
				
				/* 
				 * If we got the last entry, check it.
				 * Or, go on with the next entry.
				 */
				if (id == 0) {
					if (strcmp(long_name, dname))
						goto not_match;
					long_match = 1;
				}

				de++;
				continue;     /* Try the next entry */
			} else {
				/*
				 * It's a short entry 
				 */
				if (de->attr & 0x08) /* ignore volume labels */
					goto not_match;
				
				if (long_match == 1) {
					/* 
					 * We already have a VFAT long name 
					 * match. However, the match is only
					 * valid if the checksum matches.
					 *
					 * Turn off the long_match flag first.
					 */
					long_match = 0;
					checksum = get_checksum(de->name);
					if (checksum == vfat_csum)
						goto found;  /* Got it */
				} else {
					if (mangled_name[0] == 0) {
						/*
						 * We haven't mangled it,
						 * mangle it first.
						 */
						mangle_dos_name(mangled_name,
								dname);
					}

					if (!strncmp(mangled_name, de->name, 11))
						goto found;
				}
			}

		not_match:
			vfat_next = vfat_init;
			
			de++;
		}
		
		/* Try with the next sector */
		dir_sector = get_next_sector(this_fs, dir_sector);
		if (!dir_sector)
			return NULL;
	}
	
found:
	inode = new_fat_inode();
	inode->size = de->file_size;
	*inode->data = first_sector(de);
	inode->mode = get_fstk_mode(de->attr);

	return inode;
}

static struct inode *vfat_iget_root(void)
{
	struct inode *inode = new_fat_inode();
	int root_size = FAT_SB(this_fs)->root_size;
	
	inode->size = root_size << this_fs->sector_shift;
	*inode->data = FAT_SB(this_fs)->root;
	inode->mode = DIR_TYPE;

	return inode;
}

static struct inode *vfat_iget(char *dname, struct inode *parent)
{
	return vfat_find_entry(dname, parent);
}

/*
 * Here comes the place I don't like VFAT fs most; if we need seek
 * the file to the right place, we need get the right sector address
 * from begining everytime! Since it's a kind a signle link list, we
 * need to traver from the head-node to find the right node in that list.
 *
 * What a waste of time!
 */
static sector_t get_the_right_sector(struct file *file)
{
	int i = 0;
	int sector_pos  = file->offset >> file->fs->sector_shift;
	sector_t sector = *file->inode->data;

	for (; i < sector_pos; i++) 
		sector = get_next_sector(file->fs, sector);
	
	return sector;
}
		     
static int vfat_read(struct file *file, void *buf, int sectors)
{
	struct fstk *fs = file->fs;
	sector_t sector = get_the_right_sector(file);

	if (!sector)
		return -1;
	
	while (sectors--) {
		disk_read(fs, buf, sector, 1);
		buf += 1 << fs->sector_shift;
		
		sector = get_next_sector(fs, sector);
	}

	return 0;
}

static struct dirent * vfat_readdir(struct file *file)
{
	struct fstk *fs = file->fs;
	struct dirent *dirent;
	struct fat_dir_entry *de;
	struct fat_long_name_entry *long_de;
	struct cache_struct *cs;

	sector_t sector = get_the_right_sector(file);

	uint8_t vfat_init, vfat_next, vfat_csum;
	uint8_t id;
	int entries_left;
	int checksum;
	int long_entry = 0;
	int sec_off = file->offset & ((1 << fs->sector_shift) - 1);
	
	cs = get_cache_block(fs, sector);
	de = (struct fat_dir_entry *)(cs->data + sec_off);
	entries_left = ((1 << fs->sector_shift) - sec_off) >> 5;
	
	while (1) {
		while(entries_left--) {
			if (de->name[0] == 0)
				return NULL;
			if ((uint8_t)de->name[0] == 0xe5)
				goto invalid;
			
			if (de->attr == 0x0f) {
				/*
				 * It's a long name entry.
				 */
				long_de = (struct fat_long_name_entry *)de;
				id = long_de->id;
						
				if (id & 0x40) {
					/* init vfat_csum and vfat_init */
					vfat_csum = long_de->checksum;
					id &= 0x3f;
					vfat_init = id;

					/* ZERO the long_name buffer */
					memset(long_name, 0, sizeof long_name);
				} else {
					if (long_de->checksum != vfat_csum ||
					    id != vfat_next)
						goto invalid;
				}

				vfat_next = --id;
				
				/* got the long entry name */
				long_entry_name(long_de);
				memcpy(long_name + id * 13, entry_name, 13);
				
				if (id == 0) 
					long_entry = 1;

				de++;
				file->offset += sizeof(struct fat_dir_entry);
				continue;     /* Try the next entry */
			} else {
				/*
				 * It's a short entry 
				 */
				if (de->attr & 0x08) /* ignore volume labels */
					goto invalid;
				
				if (long_entry == 1) {
					/* Got a long entry */
					checksum = get_checksum(de->name);
					if (checksum == vfat_csum)
						goto got;
				} else {
					/* 
					 * Use the long_name buffer to store
					 * a short one.
					 */
					int i;
					char *p = long_name;
					
					for (i = 0; i < 8; i++) {
						if (de->name[i] == ' ')
							break;
						*p++ = de->name[i];
					}
					*p++ = '.';
					if (de->name[8] == ' ') {
						*--p = '\0';
					} else {
						for (i = 8; i < 11; i++) {
							if (de->name[i] == ' ')
								break;
							*p++ = de->name[i];
						}
						*p = '\0';
					}

					goto got;
				}
			}

		invalid:
			de++;
			file->offset += sizeof(struct fat_dir_entry);
		}
		
		/* Try with the next sector */
		sector = get_next_sector(fs, sector);
		if (!sector)
			return NULL;
		cs = get_cache_block(fs, sector);
		de = (struct fat_dir_entry *)cs->data;
		entries_left = 1 << (fs->sector_shift - 5);
	}

got:
	if (!(dirent = fstk_malloc(sizeof(*dirent)))) {
                malloc_error("dirent structure in ext2_readdir");
                return NULL;
        }
	dirent->d_ino = 0;           /* Inode number is invalid to FAT fs */
        dirent->d_off = file->offset;
        dirent->d_reclen = 0;
        dirent->d_type = get_fstk_mode(de->attr);
        strcpy(dirent->d_name, long_name);
	
        file->offset += sizeof(*de);  /* Update for next reading */

        return dirent;
}

static int vfat_get_blocks(struct file *file, uint32_t **blk_buf)
{
	struct inode *inode = file->inode;
	uint32_t *buf = fstk_malloc(inode->blksize);
	uint32_t *p = buf;
	sector_t block = *inode->data;
	int order = 0;

	if (!blk_buf)
		return 0;
	while (block) {
		if ((void *)p >= (void *)buf + inode->blksize * (1 << order)) {
			uint32_t *another;
			order++;
			another = fstk_malloc(inode->blksize * (1 << order));
			memcpy(another, buf, inode->blksize * (1 << order));
			fstk_free(buf);
			buf = another;
			p = (void *)buf + inode->blksize;
		}
		if (block)
			*p++ = block;
		block = get_next_sector(file->fs, block);
		
	}
	
	*blk_buf = buf;
	return p - buf;
}

static void vfat_stat(struct file *file)
{
	(void)file;
	printf("\nPlease use 'block' or 'sector' command to show this file's\n"
	       "sector address in this fs\n");
}

int vfat_guess_fs(struct fstk *fs)
{
	struct fat_bpb fat;

	disk_read(fs, &fat, 0, 1);
	if (!strncmp((char *)fat.fat12_16.fstype, "FAT", 3) ||
	    !strncmp((char *)fat.fat32.fstype, "FAT", 3))
		return 1;
	else
		return 0;
}

static void vfat_print_fs_info(struct fstk *fs)
{
	struct fat_sb_info *sbi = FAT_SB(fs);
	struct fat_bpb *fat = sbi->sb;
	char buf[32];
	uint32_t total_sectors =  fat->bxSectors ? : fat->bsHugeSectors;

	printf("VFAT fs type\t\t\t: %s\n", sbi->fat_type_str);
	printf("OEM name\t\t\t: %s\n", get_string(buf, fat->oem_name, 8));
	printf("Sector size\t\t\t: %d\n", fat->sector_size);
	printf("Total sectors\t\t\t: %d\n", total_sectors);
	printf("File system size\t\t: %d(K)\n", total_sectors >> (10 - 9));
	printf("Sectors per cluster\t\t: %d\n", fat->bxSecPerClust);
	printf("Cluster size\t\t\t: %d\n\n", sbi->clust_size);

	printf("Number of FATs\t\t\t: %d\n", fat->bxFATs);
	printf("FAT stored from sector\t\t: %d\n", (uint32_t)sbi->fat);
	printf("FAT size(in sectors)\t\t: %d\n", sbi->fat_count); 
	printf("Root dir stored from sector\t: %d\n", (uint32_t)sbi->root);
	printf("Root size(in sectors)\t\t: %d\n", sbi->root_size);
	printf("Data area stored from sector\t: %d\n\n", (uint32_t)sbi->data);
	
	printf("Resevered sectors\t\t: %d\n", fat->bxResSectors);
	printf("Sectors per track\t\t: %d\n", fat->sectors_per_track);
	printf("number of heads\t\t\t: %d\n", fat->num_heads);
	printf("number of hidden sectors\t: %d\n", fat->num_hidden_sectors);
	printf("media\t\t\t\t: 0x%02x\n\n", fat->media);

	if (sbi->fat_type == FAT32) {
		printf("extended flags\t\t\t: 0x%04x\n", 
		       fat->fat32.extended_flags);
		printf("fs version\t\t\t: 0x%04x\n", fat->fat32.fs_version);
		printf("root cluster\t\t\t: %d\n", fat->fat32.root_cluster);
		printf("Sector number of fs_info sector\t: %d\n",
		       fat->fat32.fs_info);
		printf("backup of boot sector\t\t: %d\n", 
		       fat->fat32.backup_boot_sector);
		printf("Extended boot signature\t\t: 0x%02x\n", 
		       fat->fat32.boot_sig);
		printf("Serial number(ID)\t\t: 0x%08x\n", 
		       fat->fat32.num_serial);
		printf("Volume label\t\t\t: %s\n", 
		       get_string(buf, fat->fat32.label, 11));
	} else {
		/* FAT12 or FAT16 */
		printf("Physical drive number\t\t: 0x%02x\n", 
		       fat->fat12_16.num_ph_drive);
		printf("Extended boot signature\t\t: 0x%02x\n", 
		       fat->fat12_16.boot_sig);
		printf("Serial number(ID)\t\t: 0x%08x\n", 
		       fat->fat12_16.num_serial);
		printf("Volume label\t\t\t: %s\n", 
		       get_string(buf, fat->fat12_16.label, 11));
	}
	
	printf("\n-------------------boot sector hex dump------------------\n");
	hexdump(fat, sizeof(*fat), 0);
}

static inline void vfat_type_warn(char *hoping, char *get)
{
	char got[6] = {0, };	
	
	memcpy(got, get, 5);
	printf("Warning: the vfat fs type donesn't match:"
	       "we are hoping %s, but got %s", hoping, got);
}

static void check_vfat_type(struct fat_sb_info *sbi, uint32_t clust_num)
{
	struct fat_bpb *fat = sbi->sb;
	
	if (clust_num < 4085) {
		if (strncmp((char *)fat->fat12_16.fstype, "FAT12", 5))
			vfat_type_warn("FAT12", (char *)fat->fat12_16.fstype);
		sbi->fat_type     = FAT12;
		sbi->fat_type_str = "FAT12";
	} else if (clust_num < 65525) {
		if (strncmp((char *)fat->fat12_16.fstype, "FAT16", 5))
			vfat_type_warn("FAT16", (char *)fat->fat12_16.fstype);
		sbi->fat_type     = FAT16;
		sbi->fat_type_str = "FAT16";
	} else {
		if (strncmp((char *)fat->fat32.fstype, "FAT32", 5))
			vfat_type_warn("FAT32", (char *)fat->fat32.fstype);
		sbi->fat_type     = FAT32;
		sbi->fat_type_str = "FAT32";
	}
}

static void vfat_unmount(struct fstk *fs)
{
	fstk_free(FAT_SB(fs)->sb);
}

static int vfat_mount(struct fstk *fs)
{
	struct fat_bpb *fat;
	struct fat_sb_info *sbi;
	int sectors_per_fat;
	uint32_t clust_num;
	sector_t total_sectors;
	
	fat = fstk_malloc(sizeof(*fat));
	if (!fat)
		malloc_error("fat_bpb structure");
	fs->sector_shift = 9;
	disk_read(fs, fat, 0, 1);
	
	sbi = fstk_malloc(sizeof(*sbi));
	if (!sbi)
		malloc_error("fat_sb_info structure");
	sbi->sb = fat;
	fs->fs_info = sbi;
	this_fs = fs;
	
	sectors_per_fat = fat->bxFATsecs ? : fat->fat32.bxFATsecs_32;
	total_sectors   = fat->bxSectors ? : fat->bsHugeSectors;
	
	sbi->fat       = fat->bxResSectors;
	sbi->fat_count = sectors_per_fat;	
	sbi->root      = sbi->fat + sectors_per_fat * fat->bxFATs;
	sbi->root_size = root_dir_size(fs, fat);
	sbi->data      = sbi->root + root_dir_size(fs, fat);

	sbi->clust_shift      = bsr(fat->bxSecPerClust);
	sbi->clust_byte_shift = sbi->clust_shift + fs->sector_shift;
	sbi->clust_mask       = fat->bxSecPerClust - 1;
	sbi->clust_size       = fat->bxSecPerClust << fs->sector_shift;
	
	clust_num = (total_sectors - sbi->data) >> sbi->clust_shift;
	check_vfat_type(sbi, clust_num);
	
	/* For fat fs, the cache is based on sector size */
	fs->block_shift = fs->sector_shift;
	
	return 0;
}


const struct fs_ops vfat_fs_ops = {
        .read               = vfat_read,
        .close              = NULL,
        .stat               = vfat_stat,
        .mount              = vfat_mount,
        .unmount            = vfat_unmount,
        .get_blocks         = vfat_get_blocks,
        .get_file_info      = NULL,
        .print_file_info    = NULL,
        .print_fs_info      = vfat_print_fs_info,
        .iget_root          = vfat_iget_root,
        .iget               = vfat_iget,
        .iget_by_inr        = NULL,
        .readdir            = vfat_readdir,
        .follow_symlink     = NULL
};
