/* 
 * The ext2/3/4 fs driver in fstk
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "fstk.h"
#include "file.h"
#include "dirent.h"
#include "fstklib.h"
#include "cache.h"
#include "fstk_malloc.h"
#include "ext2.h"

static struct fstk *this_fs = NULL;

/*
 * NOTE! unlike strncmp, ext2_match_entry returns 1 for success, 0 for failure.
 *
 * len <= EXT2_NAME_LEN and de != NULL are guaranteed by caller.
 */
static inline int ext2_match_entry (const char * const name,
                                    struct ext2_dir_entry * de)
{
        if (!de->d_inode)
                return 0;
	if (strlen(name) != de->d_name_len)
		return 0;
        return !strncmp(name, de->d_name, strlen(name));
}


/*
 * p is at least 6 bytes before the end of page
 */
static inline struct ext2_dir_entry *ext2_next_entry(struct ext2_dir_entry *p)
{
        return (struct ext2_dir_entry *)((char*)p + p->d_rec_len);
}

static inline struct ext2_sb_info *EXT2_SB(struct fstk *fs)
{
	return fs->fs_info;
}

struct ext2_group_desc * ext2_get_group_desc(struct fstk *fs,
                                             unsigned int group_num)
{
        struct ext2_sb_info *sbi = fs->fs_info;
	
	if (group_num >= sbi->s_groups_count) {
		printf ("ext2_get_group_desc"
                        "block_group >= groups_count - "
                        "block_group = %d, groups_count = %d",
                        group_num, sbi->s_groups_count);
                
		return NULL;
	}        
		
        return sbi->s_group_desc[group_num];
}

/* guess it's ext2/3/4 file system or not */
int ext2_guess_fs(struct fstk *fs)
{
        struct ext2_super_block sb;
        disk_read(fs, &sb, 2, 2);
        if (sb.s_magic == EXT2_SUPER_MAGIC)
                return 1;
        else
                return 0;
}



static struct ext2_dir_entry * ext2_find_entry(const char *dname, 
                                               struct inode *inode)
{
        uint32_t block;
        int i = 0;
        int index = 0;
        struct ext2_dir_entry *de;
        struct cache_struct *cs;
                
        if (!(block = bmap(this_fs, inode, index++)))
                return NULL;
        cs = get_cache_block(this_fs, block);
        de = (struct ext2_dir_entry *)cs->data;
        
        while(i < (int)inode->size) {
		if ((char *)de >= (char *)cs->data + inode->blksize) {
			if (!(block = bmap(this_fs, inode, index++)))
                                return NULL;
                        cs = get_cache_block(this_fs, block);
                        de = (struct ext2_dir_entry *)cs->data;
		}
                if (ext2_match_entry(dname, de))
                        return de;

                i += de->d_rec_len;
                de = ext2_next_entry(de);
        }
        
        return NULL;
}

static struct ext2_inode * get_inode (int inr)
{
        struct ext2_group_desc *desc;
        struct cache_struct *cs;
        uint32_t inode_group, inode_offset;
        uint32_t block_num, block_off;
        
        inr--;
        inode_group  = inr / EXT2_INODES_PER_GROUP(this_fs);
        inode_offset = inr % EXT2_INODES_PER_GROUP(this_fs);
        desc = ext2_get_group_desc (this_fs, inode_group);
        if (!desc)
                return NULL;

        block_num = desc->bg_inode_table + 
                inode_offset / EXT2_INODES_PER_BLOCK(this_fs);
        block_off = inode_offset % EXT2_INODES_PER_BLOCK(this_fs);
        
        cs = get_cache_block(this_fs, block_num);
        
        return cs->data + block_off * EXT2_SB(this_fs)->s_inode_size;
}

static void read_inode(struct ext2_inode *inode, uint32_t inr)
{
        struct ext2_inode *get;
        
        get = get_inode(inr);
        memcpy(inode, get, sizeof(*get));
}


/*
 * get the fstk's inode type, FILE , DIR, or SYMLINK
 */
static inline int get_fstk_mode(int mode)
{
        mode >>= S_IFSHIFT;
        if (mode == T_IFDIR)
                mode = DIR_TYPE;
        else if (mode == T_IFLNK)
                mode = SYMLINK_TYPE;
        else
                mode = FILE_TYPE; /* we treat others as FILE */        
        return mode;
}

static void fill_inode(struct inode *inode, struct ext2_inode *e_inode)
{     
        inode->mode    = get_fstk_mode(e_inode->i_mode);
        inode->size    = e_inode->i_size;
        inode->atime   = e_inode->i_atime;
        inode->ctime   = e_inode->i_ctime;
        inode->mtime   = e_inode->i_mtime;
        inode->dtime   = e_inode->i_dtime;
        inode->blocks  = e_inode->i_blocks;
        inode->flags   = e_inode->i_flags;
        inode->blksize = 1 << this_fs->block_shift;
        inode->file_acl = e_inode->i_file_acl;
        
        inode->data = fstk_malloc(EXT2_N_BLOCKS * sizeof(uint32_t *));
        if (!inode->data)
                malloc_error("inode data filed");
        memcpy(inode->data, e_inode->i_block, EXT2_N_BLOCKS*sizeof(uint32_t *));
}

static struct inode *ext2_iget_by_inr(uint32_t inr)
{
        struct ext2_inode e_inode;
        struct inode *inode;

        read_inode(&e_inode, inr);
        inode = fstk_malloc(sizeof(*inode));
        if (!inode) {
                malloc_error("inode structure");
                return NULL;
        } 
        fill_inode(inode, &e_inode);
        inode->ino = inr;
                
        return inode;
}

static struct inode *ext2_iget_root()
{
        return ext2_iget_by_inr(EXT2_ROOT_INO);
}

static struct inode *ext2_iget(char *dname, struct inode *parent)
{
        struct ext2_dir_entry *de;
        
        de = ext2_find_entry(dname, parent);
        if (!de)
                return NULL;
        
        return ext2_iget_by_inr(de->d_inode);
}

static int ext2_get_blocks(struct file* file, uint32_t** blk_buf)
{
        struct inode *inode = file->inode;
        int blocks = (inode->size + inode->blksize - 1) >> file->fs->block_shift;
        int index = 0;
        uint32_t block;
        uint32_t *buf = fstk_malloc((blocks + 1) * sizeof(uint32_t *));
        uint32_t *p = buf;

        if (!blk_buf)
                return 0;
        while (blocks--) {
                block = bmap(file->fs, inode, index++);
                if (block)
                        *p++ = block;
        }
        
        *blk_buf = buf;
        return p - buf;
}

static void ext2_stat(struct file *file)
{
        struct inode * inode = file->inode;
        struct fstk * fs = file->fs;
        int blocks = (inode->size + inode->blksize - 1) >> fs->block_shift;
        int i = 0, index = 0;
        uint32_t block;
        
        if (inode->flags & EXT4_EXTENTS_FLAG)
                printf("Finding the block address by extent\n");
        else
                printf("Finding the block address by traditional way\n");
        
        printf("Blocks: (total:%d)\n", blocks);
        for (; i < blocks; i++) {
                uint32_t start;
                uint32_t last_block;
                int bits = fs->block_shift - 2;
                int adds = 1 << bits;
                int adds2 = 1 << (bits * 2); 
                
                if (index == 12) 
                        printf("(IND):%u  ", ((uint32_t *)inode->data)[12]);
                else if (index == 12 + adds)
                        printf("(DND):%u  ", ((uint32_t *)inode->data)[13]);
                else if (index == 12 + adds + adds2)
                        printf("(TND):%u  ", ((uint32_t *)inode->data)[14]);
                
                if (!(block = bmap(fs, inode, index++)))
                        continue;
                start = last_block = block;
                while(last_block + 1 == (block = bmap(fs, inode, index))) {
                        last_block++;
                        index++;
                }
                
                if (last_block == start)
                        printf("(%d):%u  ", index - 1, start);
                else
                        printf("(%d-%d):%u-%u  ", i, index - 1, 
                               start, last_block);
                
                i += last_block - start;
        }
        printf("\n");
}

int ext2_read(struct file* file, void *buf, int blocks)
{
        struct inode *inode = file->inode;
        struct fstk *fs = file->fs;
        uint32_t index = file->offset >> fs->block_shift;
        uint32_t block;
                
        while (blocks--) {
                if (!(block = bmap(fs, inode, index++))) {
                        printf("WARNING: something wrong happened at bmap, "
                               "maybe your fs is corrupted\n");
                        return -1;
                }
                
                block_read(fs, buf, block, 1);
                buf += inode->blksize;
        }
        
        return 0;
}


char * ext2_follow_symlink(struct inode *inode, const char *name_left)
{
        int sec_per_block = 1 << (this_fs->block_shift - this_fs->sector_shift);
        int fast_symlink;
        char *symlink_buf;
        char *p;
        struct cache_struct *cs;

        symlink_buf = fstk_malloc(inode->blksize);
        if (!symlink_buf) {
                malloc_error("symlink buffer");
                return NULL;
        }
        fast_symlink = (inode->file_acl ? sec_per_block : 0) == inode->blocks;
        if (fast_symlink) {
                memcpy(symlink_buf, inode->data, inode->size);
        } else {
                cs = get_cache_block(this_fs, *(uint32_t *)inode->data);
                memcpy(symlink_buf, cs->data, inode->size);
        }
        p = symlink_buf + inode->size;
        
        if (*name_left)
                *p++ = '/';
        strcpy(p, name_left);
        if(!(p = fstk_strdup(symlink_buf)))
                return symlink_buf;

        fstk_free(symlink_buf);        
        return p;
}

static struct dirent * ext2_readdir(struct file *file)
{
        struct fstk *fs = file->fs;
        struct inode *inode = file->inode;
        struct dirent *dirent;
        struct ext2_dir_entry *de;
        struct cache_struct *cs;
        int index = file->offset >> fs->block_shift;
        uint32_t block;
                
	if (file->offset >= inode->size)
		return NULL;
        if (!(block = bmap(fs, inode, index)))
                return NULL;        
        cs = get_cache_block(fs, block);
        de = (struct ext2_dir_entry *)(cs->data + (file->offset & (inode->size - 1)));
        
        if (!(dirent = fstk_malloc(sizeof(*dirent)))) {
                malloc_error("dirent structure in ext2_readdir");
                return NULL;
        }
        dirent->d_ino = de->d_inode;
        dirent->d_off = file->offset;
        dirent->d_reclen = de->d_rec_len;
        dirent->d_type = de->d_file_type;
        memcpy(dirent->d_name, de->d_name, de->d_name_len);
	dirent->d_name[de->d_name_len] = '\0';
	
        file->offset += de->d_rec_len;  /* Update for next reading */

        return dirent;
}
                
        
        
static int ext2_mount(struct fstk *fs)
{
        struct ext2_sb_info *sbi;
        struct ext2_super_block *esb;
        int block_size;
        int db_count;
        int i;
        int desc_block;
        char *desc_buffer;

        this_fs = fs;             /* get the current fs pointer */
                
        esb = fstk_malloc(sizeof(struct ext2_super_block));
        if (!esb)
                malloc_error("ext2_super_block structure");
        fs->sector_shift = 9;     /* should set it before any disk operations */
        disk_read(fs, esb, 2, 2); /* read the super block from disk */
        
        sbi = fstk_malloc(sizeof(*sbi));
        if (!sbi)
                malloc_error("ext2_sb_info structure");     
        fs->fs_info = sbi;
        sbi->s_es = esb;

        if (esb->s_magic != EXT2_SUPER_MAGIC) {
                printf("ext2 mount error: can't found ext2 file system!\n");
                exit(0);
        }

        fs->block_shift  = esb->s_log_block_size + 10;
        block_size = 1 << fs->block_shift;

        sbi->s_inodes_per_group = esb->s_inodes_per_group;
        sbi->s_blocks_per_group = esb->s_blocks_per_group;
        sbi->s_inodes_per_block = block_size / esb->s_inode_size;
        sbi->s_itb_per_group    = sbi->s_inodes_per_group / 
                                  sbi->s_inodes_per_block;
        if (esb->s_desc_size < sizeof(struct ext2_group_desc))
                esb->s_desc_size = sizeof(struct ext2_group_desc);
        sbi->s_desc_per_block   = block_size / esb->s_desc_size;
        sbi->s_groups_count     = (esb->s_blocks_count - 
                                   esb->s_first_data_block +
				   EXT2_BLOCKS_PER_GROUP(fs) - 1) /
				   EXT2_BLOCKS_PER_GROUP(fs);
	db_count = (sbi->s_groups_count + EXT2_DESC_PER_BLOCK(fs) - 1) /
		   EXT2_DESC_PER_BLOCK(fs);
        sbi->s_gdb_count = db_count;
        sbi->s_inode_size = esb->s_inode_size;

        /* read the descpritors */
        desc_block = esb->s_first_data_block + 1;
        desc_buffer = fstk_malloc(db_count * block_size);
        if (!desc_buffer)
                malloc_error("desc_buffer");
        block_read(fs, desc_buffer, desc_block, db_count);
        sbi->s_group_desc = fstk_malloc(sizeof(struct ext2_group_desc *) 
                                   * sbi->s_groups_count);
        if (!sbi->s_group_desc)
                malloc_error("sbi->s_group_desc");
        for (i = 0; i < (int)sbi->s_groups_count; i++) {
                sbi->s_group_desc[i] = (struct ext2_group_desc *)desc_buffer;
                desc_buffer += esb->s_desc_size;
        }
        
        /* count's sector address from block 0 */
        fs->base = 0;

        return 0;
}

void ext2_unmount(struct fstk *fs)
{
        struct ext2_sb_info *sbi = EXT2_SB(fs);
        void *desc_buffer = (void *)sbi->s_group_desc[0];
        
        fstk_free(sbi->s_group_desc);
        fstk_free(desc_buffer);
        fstk_free(sbi->s_es);
}


void ext2_print_fs_info(struct fstk *fs)
{
        int i;
        struct ext2_sb_info *sbi = EXT2_SB(fs);
        
        printf("file system: %s\n", fs->fs_name);
        printf("block size : %d\n", 1 << fs->block_shift);
        printf("inode size : %d\n", sbi->s_inode_size);
        printf("desc  size : %d\n", sbi->s_es->s_desc_size);
       
        printf("\nfirst data block: %d\n", sbi->s_es->s_first_data_block);
        printf("inodes per block: %d\n", sbi->s_inodes_per_block);
        printf("inodes per group: %d\n", sbi->s_inodes_per_group);
        printf("inode table blocks: %d\n", sbi->s_itb_per_group);
        printf("blocks per group: %d\n", sbi->s_blocks_per_group);
        printf("group desc per block: %d\n", sbi->s_desc_per_block);
        printf("group descriptor blocks: %d\n", sbi->s_gdb_count);
        printf("groups count: %d\n", sbi->s_groups_count);

        for(i = 0; i < (int)sbi->s_groups_count; i++)
                hexdump(sbi->s_group_desc[i], sbi->s_es->s_desc_size, 0);
}
        
        


const struct fs_ops ext2_fs_ops = {
        .read               = ext2_read,
        .close              = NULL,
        .stat               = ext2_stat,
        .mount              = ext2_mount,
        .unmount            = ext2_unmount,
        .get_blocks         = ext2_get_blocks,
        .get_file_info      = NULL,
        .print_file_info    = NULL,
        .print_fs_info      = ext2_print_fs_info,
        .iget_root          = ext2_iget_root,
        .iget               = ext2_iget,
        .iget_by_inr        = ext2_iget_by_inr,
        .readdir            = ext2_readdir,
        .follow_symlink     = ext2_follow_symlink
};
