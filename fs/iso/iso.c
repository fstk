/* 
 * The vfat fs driver in fstk
 *
 * Mostly based on the iso9660 fs  driver from Syslinux project.
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "fstk.h"
#include "file.h"
#include "dirent.h"
#include "fstklib.h"
#include "cache.h"
#include "fstk_malloc.h"
#include "iso.h"

static struct fstk *this_fs = NULL;

static struct inode *new_iso_inode(void)
{
	struct inode *inode = fstk_malloc(sizeof(*inode));
	
	if (!inode) {
		malloc_error("inode structure in new_iso_inode");
		return NULL;
	}
	memset(inode, 0, sizeof(*inode));

	inode->data = fstk_malloc(sizeof(uint32_t));
	if (!inode) {
		malloc_error("inode->data in new_iso_inode");
		fstk_free(inode);
		return NULL;
	}

	inode->blksize = BLOCK_SIZE(this_fs);
	
	return inode;
}

static inline struct iso_sb_info * ISO_SB(struct fstk *fs)
{
	return fs->fs_info;
}


static int iso_convert_name(char *dst, char *src, int len)
{
	int i = 0;
	char c;
	
	for (; i < len; i++) {
		c = src[i];
		if (!c)
			break;
		
		/* remove ';1' in the end */
		if (c == ';' && i == len - 2 && src[i + 1] == '1')
			break;
		/* convert others ';' to '.' */
		if (c == ';')
			c = '.';
		*dst++ = c;
	}

	/* Then remove the terminal dots */
	while (*(dst - 1) == '.') {
		if (i <= 2)
			break;
		dst--;
		i--;
	}
	*dst = 0;
	
	return i;
}

static int iso_compare_name(char *de_name, int len, char *file_name)
{
	char iso_file_name[256];
	char *p = iso_file_name;
	char c1, c2;
	int i;

	i = iso_convert_name(iso_file_name, de_name, len);
	
	if (i != (int)strlen(file_name))
		return 0;
	
	while (i--) {
		c1 = *p++;
		c2 = *file_name++;
		
		/* convert to lower case */
		c1 |= 0x20;
		c2 |= 0x20;
		if (c1 != c2)
			return 0;
	}
	
	return 1;
}


/*
 * Find a entry in the specified dir with name _dname_.
 */
static struct iso_dir_entry *iso_find_entry(char *dname, struct inode *inode)
{
	sector_t dir_block = *inode->data;
	int i = 0, offset = 0;
	char *de_name;
	int de_name_len, de_len;
	struct iso_dir_entry *de;
	struct iso_dir_entry tmpde;
	struct cache_struct *cs = NULL;
	
	while (1) {
		if (!cs) {
			if (++i > inode->blocks)
				return NULL;
			cs = get_cache_block(this_fs, dir_block++);
			de = (struct iso_dir_entry *)cs->data;
			offset = 0;
		}
		de = (struct iso_dir_entry *)(cs->data + offset);

		de_len = de->length;
		if (de_len == 0) {    /* move on to the next block */
			cs = NULL;
			continue;
		}
		offset += de_len;
		
		/* Make sure we have a full directory entry */
		if (offset >= inode->blksize) {
			int slop = de_len + inode->blksize - offset;
			
			memcpy(&tmpde, de, slop);
			offset &= inode->blksize - 1;
			if (offset) {
				if (++i > inode->blocks)
					return NULL;
				cs = get_cache_block(this_fs, dir_block++);
				memcpy((void *)&tmpde + slop, cs->data, offset);
			}
			de = &tmpde;
		}
		
		if (de_len < 33) {
			printf("Corrupted directory entry in sector %u\n", 
			       (uint32_t)(dir_block - 1));
			return NULL;
		}
		
		de_name_len = de->name_len;
		de_name = de->name;
		/* Handling the special case ".' and '..' here */
		if((de_name_len == 1) && (*de_name == 0)) {
			de_name = ".";
		} else if ((de_name_len == 1) && (*de_name == 1)) {
			de_name ="..";
			de_name_len = 2;
		}
		if (iso_compare_name(de_name, de_name_len, dname))
			return de;
	}
}

static inline int get_fstk_mode(uint8_t flags)
{
	if (flags & 0x02)
		return DIR_TYPE;
	else
		return FILE_TYPE;
}

static struct inode *iso_get_inode(struct iso_dir_entry *de)
{
	struct inode *inode = new_iso_inode();
	
	if (!inode)
		return NULL;
	inode->mode   = get_fstk_mode(de->flags);
	inode->size   = *(uint32_t *)de->size;
	*inode->data  = *(uint32_t *)de->extent;
	inode->blocks = (inode->size + SECTOR_SIZE(this_fs) - 1) 
		>> this_fs->sector_shift;
	
	return inode;
}


static struct inode *iso_iget_root(void)
{
	struct inode *inode = new_iso_inode();
	struct iso_dir_entry *root = &ISO_SB(this_fs)->root;

	if (!inode) 
		return NULL;
	
	inode->mode   = DIR_TYPE;
	inode->size   = *(uint32_t *)root->size;
	*inode->data  = *(uint32_t *)root->extent;
	inode->blocks = (inode->size + (1 << this_fs->sector_shift) - 1) 
		>> this_fs->sector_shift;
	
	return inode;
}	

static struct inode *iso_iget(char *dname, struct inode *parent)
{
	struct iso_dir_entry *de;
	
	de = iso_find_entry(dname, parent);
	if (!de)
		return NULL;
	
	return iso_get_inode(de);
}

static int iso_read(struct file *file, void *buf, int blocks)
{
	struct fstk *fs = file->fs;
	block_t block = *file->inode->data + (file->offset >> fs->block_shift);

	disk_read(fs, buf, block, blocks);
	return 0;
}


static struct dirent *iso_readdir(struct file *file)
{
	struct fstk *fs = file->fs;
	struct inode *inode = file->inode;
	struct iso_dir_entry *de, tmpde;
	struct dirent *dirent;
	struct cache_struct *cs = NULL;
	block_t block =  *file->inode->data + (file->offset >> fs->block_shift);
	int offset = file->offset & (BLOCK_SIZE(fs) - 1);
	int i = 0;
	int de_len, de_name_len;
	char *de_name;
	
	while (1) {
		if (!cs) {
			if (++i > inode->blocks)
				return NULL;
			cs = get_cache_block(this_fs, block++);
			de = (struct iso_dir_entry *)cs->data;
		}
		de = (struct iso_dir_entry *)(cs->data + offset);

		de_len = de->length;
		if (de_len == 0) {    /* move on to the next block */
			cs = NULL;
			file->offset = (file->offset + BLOCK_SIZE(fs) - 1)
				>> fs->block_shift;
			continue;
		}
		offset += de_len;
		
		/* Make sure we have a full directory entry */
		if (offset >= inode->blksize) {
			int slop = de_len + inode->blksize - offset;
			
			memcpy(&tmpde, de, slop);
			offset &= inode->blksize - 1;
			if (offset) {
				if (++i > inode->blocks)
					return NULL;
				cs = get_cache_block(this_fs, block++);
				memcpy((void *)&tmpde + slop, cs->data, offset);
			}
			de = &tmpde;
		}
		
		if (de_len < 33) {
			printf("Corrupted directory entry in sector %u\n", 
			       (uint32_t)(block - 1));
			return NULL;
		}
		
		de_name_len = de->name_len;
		de_name = de->name;
		/* Handling the special case ".' and '..' here */
		if((de_name_len == 1) && (*de_name == 0)) {
			de_name = ".";
		} else if ((de_name_len == 1) && (*de_name == 1)) {
			de_name ="..";
			de_name_len = 2;
		}
		
		break;
	}
	
	if (!(dirent = fstk_malloc(sizeof(*dirent)))) {
		malloc_error("dirent structure in ext2_readdir");
                return NULL;
        }
	
	dirent->d_ino = 0;           /* Inode number is invalid to ISO fs */
        dirent->d_off = file->offset;
        dirent->d_reclen = de_len;
        dirent->d_type = get_fstk_mode(de->flags);
        iso_convert_name(dirent->d_name, de_name, de_name_len);
	tolower_str(dirent->d_name);
	
        file->offset += de_len;  /* Update for next reading */

        return dirent;
}

static void iso_unmount(struct fstk *fs)
{
	/* Don't need do anything now */
	(void)fs;
}

static int iso_mount(struct fstk *fs)
{
	struct iso_sb_info *sbi;
	char buf[2048];
		
	fs->sector_shift = 11;
	disk_read(fs, buf, 16, 1);
	
	sbi = fstk_malloc(sizeof(*sbi));
	if (!sbi) {
		malloc_error("iso_sb_info structure");
		return 1;
	}
	fs->fs_info = sbi;
	this_fs = fs;
	
	memcpy(&sbi->root, buf + ROOT_DIR_OFFSET, sizeof(sbi->root));

	fs->block_shift = fs->sector_shift;
	return 0;
}
	
	
const struct fs_ops iso_fs_ops = {
        .read               = iso_read,
        .close              = NULL,
        .stat               = NULL, //iso_stat,
        .mount              = iso_mount,
        .unmount            = iso_unmount,
        .get_blocks         = NULL, //iso_get_blocks,
        .get_file_info      = NULL,
        .print_file_info    = NULL,
        .print_fs_info      = NULL, //iso_print_fs_info,
        .iget_root          = iso_iget_root,
        .iget               = iso_iget,
        .iget_by_inr        = NULL,
        .readdir            = iso_readdir,
        .follow_symlink     = NULL
};
