/*
 * This file is used to get the sectors and blocks address that used
 * to store a file.
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include "fstk.h"
#include "file.h"
#include "fstk_malloc.h"


static void print_data(uint32_t *data, int n)
{
        uint32_t start = *data;
        uint32_t end = start;
        int counts = 0;
        
        while (n--) {
                while(++end == *++data) {
                        counts++;
                        n--;
                }
                if (counts) 
                        printf("%d-%d ", start, end-1);
                else 
                        printf("%d ", start);
                
                start  = end = *data;                
                counts = 0;
        }
        printf("\n");
}

/*
 * Show the file's block address, print by print_data function.
 */
void fstk_block(struct fstk *fs, int argc, char *argv[])
{
        int i = 1;
        int blocks;
        uint32_t *block;
        struct file *file;

        for (; i < argc; i++) {
                if (!(file = fstk_open(fs, argv[i]))) {
                        printf("block: can't access %s: "
                               "No such file or diectory\n", argv[i]);
                        continue;
                }
                if (!(blocks = fs->ops->get_blocks(file, &block)))
                        return;
                printf("\n%s:\n", argv[i]);
                print_data(block, blocks);
                
                fstk_free(block);
                fstk_close(file);
        }
}

int blk_to_sec(struct fstk *fs, uint32_t **sector, uint32_t *block, int blocks)
{
        int blks = fs->block_shift - fs->sector_shift;
        int i = 0;
        uint32_t *sec_buf = fstk_malloc((blocks << blks) * sizeof(uint32_t *));
        uint32_t *p = sec_buf;
        
        if (!sec_buf)
                return 0;
        while (blocks--) {
                for (i = 0; i < 1 << blks; i++)
                        *p++ = fs->base + (*block << blks) + i;
                block++;
        }
        
        *sector = sec_buf;
        return p - sec_buf;
}


/*
 * The same as fstk_block, but show sector address
 */
void fstk_sector(struct fstk *fs, int argc, char *argv[])
{
        int i = 1;
        uint32_t blocks, sectors;
        uint32_t *block;
        uint32_t *sector;
        struct file *file;

        for (; i < argc; i++) {
                if (!(file = fstk_open(fs, argv[i]))) {
                        printf("sector: can't access %s: "
                               "No such file or diectory\n", argv[i]);
                        continue;
                }
                if (!(blocks = fs->ops->get_blocks(file, &block)))
                        return;                
                sectors = blk_to_sec(fs, &sector, block, blocks);
                printf("\n%s:\n", argv[i]);
                print_data(sector, sectors);
                
                fstk_free(sector);
                fstk_free(block);
                fstk_close(file);
        }
}
