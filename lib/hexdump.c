/*
 * The hex dump lib.
 * 
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

void hexdump(void *data, int len, int base)
{
        int i = 0;
        int index = 0;
        uint8_t *p = data;
        uint8_t hex[16 * 3 + 1] = {0, };
        uint8_t text[16 + 1] = {0, };
        uint8_t c;
        
        for (i = 0; i < len; i++) {
                index = i & 0xf;                               
                if (i && index == 0) {
                        /* print the buffer before reset it */
                        printf("%08x  %-48s  |%-16s|\n", base, hex, text);
                        base += 0x10;
                        memset(hex, 0, sizeof hex);
                        memset(text, 0, sizeof text);
                }
                
                c = *p++;
                sprintf((char *)&hex[index * 3], "%02x ", c);
                if (c < 0x20 || c > 0x7f)
                        text[index] = '.';
                else
                        text[index] = c;
        }
        
        /* print the last part */
        printf("%08x  %-48s  |%-16s|\n", base, hex, text);
}
