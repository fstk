/* 
 * Others function used in fstk
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "fstk.h"
#include "fstklib.h"
#include "fstk_malloc.h"

inline int bsr(uint32_t num)
{
    asm("bsrl %1,%0" : "=r" (num) : "rm" (num));
    return num;
}


/*
 * In fact, _data_ is mostly like a buffer, well the _ptr_ points some 
 * important data that needed returened, and we should free the buffer,
 * while should not delete the data pointed by _ptr_. So, my plan is 
 * allocate a small memory for _ptr_ as _len_ bytes, then free the buffer.
 */
void * free_alloc_mem(void *data, void *ptr, int len)
{
        void *tmp = fstk_malloc(len);
        if (!tmp) {
                malloc_error("tmp swap memory in free_mem");
                return NULL;
        }

        memcpy(tmp, ptr, len);
        fstk_free(data);
        
        return tmp;
}

/*
 * The strcpy of fstk version, just like strcpy, but just copy CHARs,
 * means would stop at meeting a char with ASCII below ' '.
 */
int fstk_strccpy(char *dst, const char *src)
{
        char *p = dst;
        while (*src >= ' ') 
                *p++ = *src++;
        *p = '\0';

        return p - dst;
}


/* The fstk strdup version */
char *fstk_strdup(const char *str)
{
        char *res = fstk_malloc(strlen(str));
 
        if (res)
                strcpy(res, str);
        return res;
}

/* Convert to lower case string */
void tolower_str(char *str)
{
	while (*str) {
		if (*str >= 'A' && *str <= 'Z')
			*str = *str + 0x20;
		str++;
	}
}
