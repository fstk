/*
 * The disk IO interface.
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "fstk.h"

uint32_t disk_read(struct fstk *fs, void *buf, 
                   uint32_t sector, uint32_t sectors)
{
        int bytes_read;
        
        if (lseek(fs->fd, sector << fs->sector_shift, SEEK_SET) < 0) {
                printf("Seek file system %s error...\n", fs->fs_name);
                exit(0);
        }

        bytes_read = read(fs->fd, buf, sectors << fs->sector_shift);
        if (bytes_read < 0) {
                printf("Read file from fs %s error...\n", fs->fs_name);
                exit(0);
        }

        return bytes_read;
}

/* block read function */
uint32_t block_read(struct fstk *fs, void *buf,
                    uint32_t block, uint32_t blocks)
{
        return disk_read(fs, buf, block << (fs->block_shift - fs->sector_shift),
                         blocks << (fs->block_shift - fs->sector_shift));
}


uint32_t disk_write(struct fstk *fs, void *buf,
                    uint32_t sector, uint32_t sectors)
{
        int bytes_write;

        if (lseek(fs->fd, sector << fs->sector_shift, SEEK_SET) < 0) {
                printf("Seek file system %s error...\n", fs->fs_name);
                exit(0);
        }

        bytes_write = write(fs->fd, buf, sectors << fs->sector_shift);
        if (bytes_write < 0) {
                printf("Write file into fs %s error...\n", fs->fs_name);
                exit(0);
        }

        return bytes_write;
}

uint32_t block_write(struct fstk *fs, void *buf,
                     uint32_t block, uint32_t blocks)
{
        return disk_write(fs, buf, 
                          block << (fs->block_shift - fs->sector_shift),
                          blocks << (fs->block_shift - fs->sector_shift));
}
