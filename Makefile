## -----------------------------------------------------------------------
##
##   Copyright 2009 Liu Aleaxander - All Rights Reserved
##
##   This program is free software; you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation, Inc., 53 Temple Place Ste 330,
##   Boston MA 02111-1307, USA; either version 2 of the License, or
##   (at your option) any later version; incorporated herein by reference.
##
## -----------------------------------------------------------------------

#
# Make file for fstk project; it's really simple.
#

INCLUDE = -I/usr/include -Iinclude
CFLAGS 	= -g -Wall -W -lefence $(INCLUDE)
CSRC 	:= $(wildcard *.c */*.c fs/*/*.c)
OBJS	:= $(patsubst %.c,%.o,$(CSRC))

fstk:$(OBJS)
	gcc  -lreadline -o fstk $(OBJS)

$(OBJS): %.o: %.c
	gcc $(CFLAGS) -c $< -o $@

install:
	cp fstk /usr/bin/

clean:
	rm -f *~ *.o */*.o fstk */*~ fs/*/*~

dep:
	sed '/\#\#\# Dependencies/q' < Makefile > tmp_make
	(for i in $(CSRC);do gcc $(INCLUDE) -MM $$i;done) >> tmp_make
	cp tmp_make Makefile; rm tmp_make

### Dependencies
block.o: block.c include/fstk.h include/file.h include/fstk_malloc.h
cache.o: cache.c include/fstk.h include/fstklib.h include/cache.h
cat.o: cat.c include/fstk.h include/fstklib.h include/file.h
cd.o: cd.c include/fstk.h include/file.h include/dirent.h \
  include/fstk_malloc.h
check_fat.o: check_fat.c include/fstk.h
dir.o: dir.c include/fstk.h include/file.h include/dirent.h \
  include/fstk_malloc.h
file.o: file.c include/fstk.h include/file.h include/dirent.h \
  include/fstklib.h include/fstk_malloc.h
fs.o: fs.c include/fstk.h include/fs.h include/fstk.h include/fstklib.h \
  include/fstk_malloc.h
fstk.o: fstk.c version.h include/fstk.h include/fs.h include/fstk.h \
  include/file.h include/fstklib.h include/fstk_malloc.h include/cache.h \
  include/command.h
ls.o: ls.c include/fstk.h include/file.h include/dirent.h
malloc.o: malloc.c include/fstk_malloc.h
stat.o: stat.c include/fstk.h include/file.h include/dirent.h \
  include/fstklib.h
diskio.o: lib/diskio.c include/fstk.h
hexdump.o: lib/hexdump.c
utils.o: lib/utils.c include/fstk.h include/fstklib.h \
  include/fstk_malloc.h
bmap.o: fs/ext2/bmap.c fs/ext2/ext2.h include/fstk.h include/fstklib.h \
  include/cache.h
ext2.o: fs/ext2/ext2.c include/fstk.h include/file.h include/dirent.h \
  include/fstklib.h include/cache.h include/fstk_malloc.h fs/ext2/ext2.h
fat.o: fs/fat/fat.c include/fstk.h include/file.h include/dirent.h \
  include/fstklib.h include/cache.h include/fstk_malloc.h fs/fat/fat.h
misc.o: fs/fat/misc.c include/fstk.h include/fstklib.h include/cache.h \
  include/fstk_malloc.h fs/fat/fat.h
iso.o: fs/iso/iso.c include/fstk.h include/file.h include/dirent.h \
  include/fstklib.h include/cache.h include/fstk_malloc.h fs/iso/iso.h
