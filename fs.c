/* 
 * The fs related functions, like fstk_mount/unmount and so on.
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "fstk.h"
#include "fs.h"
#include "fstklib.h"
#include "fstk_malloc.h"

char *guess_fs(char *fs)
{
        int fd;
        char *type;
        struct fstk fstk;

        fd = open(fs, O_RDONLY);
        if (fd < 0) {
                printf("No such fs: %s not found\n", fs);
                return NULL;
        }
        
        fstk.fd = fd;
        fstk.sector_shift = 9;
        
        if (ext2_guess_fs(&fstk))
                type = "ext";
        else if (vfat_guess_fs(&fstk))
                type = "vfat";
        else
		type = NULL;
	
        close(fd);
        return type;
}


int fstk_mount(struct fstk *fstk, char *fs)
{
        fstk->fd = open(fs, O_RDWR);
        if (fstk->fd < 0) {
                printf("open file system %s error...\n", fs);
                exit(0);
        }

        return fstk->ops->mount(fstk);
}

void fstk_unmount(struct fstk *fs)
{
        if (!fs)
                return;
        close(fs->fd);
        if (!fs->ops)
                goto free_fs;
        fs->ops->unmount(fs);
        fstk_free(fs->fs_info);
free_fs:
        fstk_free(fs);
}
        


struct fstk * init_fs(char *fs_type)
{
        struct fstk *fs;
        
        fs = fstk_malloc(sizeof (struct fstk));
        if (!fs)
                malloc_error("fstk structure");
        
        /* ext, ext2, ext3, and ext4 stands the same thing */
        if (!strncmp(fs_type, "ext", 3))
                fs->ops = &ext2_fs_ops;
        else if (!strcmp(fs_type, "fat") || !strcmp(fs_type, "vfat"))
                fs->ops = &vfat_fs_ops;
        else if (!strncmp(fs_type, "iso", 3))
                fs->ops = &iso_fs_ops;
        else
                return NULL;

        strcpy(fs->fs_name, fs_type);
        return fs;
}

