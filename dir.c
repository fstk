/*
 * All the functions related with dir, like open/read/closedir, are
 * implemented here.
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * can be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include "fstk.h"
#include "file.h"
#include "dirent.h"
#include "fstk_malloc.h"


DIR * this_dir = NULL;

DIR * fstk_opendir(struct fstk *fs, char *name)
{
        DIR *dir = fstk_malloc(sizeof(*dir));

        if (!dir) {
                malloc_error("DIR structure");
                return NULL;
        }
        dir->dd_dir = fstk_open(fs, name);
        if (!dir->dd_dir) {
                fstk_free(dir);
                return NULL;
        }

        return dir;
}
        

struct dirent *fstk_readdir(DIR *dir)
{
        struct fstk *fs = dir->dd_dir->fs;
        
        return fs->ops->readdir(dir->dd_dir);
}

        
void fstk_closedir(DIR *dir)
{
        if (!dir)
                return;
        fstk_close(dir->dd_dir);
        fstk_free(dir);
}

        
void set_current_dentry(struct fstk *fs)
{
        if (!(this_dir = fstk_opendir(fs, "/")))
                printf("Warning: setting this_dir error\n");
}

void free_current_dentry(void)
{
        if (this_dir)
                fstk_closedir(this_dir);
}
