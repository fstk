/*
 * The simple ls command implemention.
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * can be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "fstk.h"
#include "file.h"
#include "dirent.h"


static const char *month[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                               "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

static void ls_entry(struct inode *inode, char * de_name)
{
        struct tm *tm;
        time_t mtime;
        char data[80];
        
        mtime = inode->mtime;
        tm = localtime(&mtime);
        sprintf(data, "%6u %10u  %2d-%s-%4d %02d:%02d  %s",
                inode->ino, inode->size, tm->tm_mday, 
                month[tm->tm_mon], 1900 + tm->tm_year, 
                tm->tm_hour, tm->tm_min, de_name);
        
        printf("%s\n", data);
}
        
void fstk_ls(struct fstk *fs, int argc, char *argv[])
{
        DIR *dir;
        struct dirent *de;
        struct inode *inode;
        int i = 1;
        
        /* No argument received, just list the current directory */
        if (argc == 1) {
                this_dir->dd_dir->offset = 0; /* Make sure we read from start */
		while((de = fstk_readdir(this_dir))) {
			if (fs->ops->iget_by_inr) {
				inode = fs->ops->iget_by_inr(de->d_ino);
				ls_entry(inode, de->d_name);
				free_inode(inode);
			} else {
				printf("%s\n", de->d_name);
			}
			fstk_free(de);
                }
                return;
        }

        for (; i < argc; i++) {
                if (!(dir = fstk_opendir(fs, argv[i]))) {
                        printf("can't access %s: " 
                               "No such file or directory\n", argv[i]);
                        continue;
                }
                if (dir->dd_dir->inode->mode != DIR_TYPE) {
			if (fs->ops->iget_by_inr) {
				inode = fs->ops->iget_by_inr(dir->dd_dir->inode->ino);
				ls_entry(inode, argv[i]);
				free_inode(inode);
			} else {
				printf("%s\n", de->d_name);
			}
                        goto next;
                }                
		
		if (argc > 2)
			printf("%s:\n", argv[i]);
                while((de = fstk_readdir(dir))) {
			if (fs->ops->iget_by_inr) {
				inode = fs->ops->iget_by_inr(de->d_ino);
				ls_entry(inode, de->d_name);
				free_inode(inode);
			} else {
				printf("%s\n", de->d_name);
			}

			fstk_free(de);
                }                
        next:
                printf("\n");
                fstk_closedir(dir);
        }
}
        
