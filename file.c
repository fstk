/*
 * All functions related with file, such as open, close, lseek and so on.
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "fstk.h"
#include "file.h"
#include "dirent.h"
#include "fstklib.h"
#include "fstk_malloc.h"


struct file *fstk_open(struct fstk *fs, const char *name)
{
        struct file *file;
        struct inode *inode;
        struct inode *parent;
        char part[256];
        char *p;
        int symlink_count = 64;

        if (*name == '/') {
                inode = fs->ops->iget_root();
                while(*name == '/')
                        name++;
        } else {
                inode = this_dir->dd_dir->inode;
        }
        parent = inode;
        
        while (*name) {
                p = part;
                while(*name && *name != '/')
                        *p++ = *name++;
                *p = '\0';
                inode = fs->ops->iget(part, parent);
                if (!inode)
                        return NULL;
                if (inode->mode == SYMLINK_TYPE) {
                        if (--symlink_count == 0 ||            /* limit check */
                            (int)inode->size >= inode->blksize)
                                return NULL;
                        name = fs->ops->follow_symlink(inode, name);
                        free_inode(inode);
                        continue;
                }
                if (parent != this_dir->dd_dir->inode)
                        free_inode(parent);
                parent = inode;
                if (! *name)
                        break;
                while(*name == '/')
                        name++;
        }
        
        file = fstk_malloc(sizeof(*file));
        if (!file) {
                malloc_error("file strucutre");
                return NULL;
        }
        file->fs     = fs;
        file->inode  = inode;
        file->offset = 0;

        return file;
}

#define MIN(a,b) ((a) < (b) ? (a) : (b))

int fstk_read(struct file *file, void *buf, uint32_t len)
{
        struct inode *inode = file->inode;
        struct fstk *fs = file->fs;
        uint32_t bytes_need = MIN(len, inode->size - file->offset);
        int blocks = (bytes_need + inode->blksize - 1) >> fs->block_shift;
        
        if (!bytes_need)
                return -1;
        if ((fs->ops->read(file, buf, blocks)) < 0)
                return -1;
        memmove(buf, buf + (file->offset & (inode->blksize -1)), bytes_need);
        file->offset += bytes_need;
        
        return bytes_need;
}

/*
 * lseek function, use the macro from unistd.h. Well, it aslo
 * would be easy if you want define your macros, like FSTK_SEEK_
 */
int fstk_lseek(struct file *file, int off, int mode)
{
        if (mode == SEEK_CUR)
                file->offset += off;
        else if (mode == SEEK_END)
                file->offset = file->inode->size + off;
        else if (mode == SEEK_SET)
                file->offset = off;
        else
                file->offset = -1;
        return file->offset;
}

void fstk_close(struct file *file)
{
        if (file) {
                if (file->inode->data)
                        fstk_free(file->inode->data);
                fstk_free(file->inode);
                fstk_free(file);
        }
}
