#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "version.h"
#include "fstk.h"
#include "fs.h"
#include "file.h"
#include "fstklib.h"
#include "fstk_malloc.h"
#include "cache.h"
#include "command.h"

#include <readline/readline.h>
#include <readline/history.h>


static int done = 0;


static inline void fstk_shell_help(void)
{
        printf("fs                --open a new file sytem\n"
               "help              --print this information\n"
               "print [fs]        --print the current fs information\n"
               "quit, q           --quit the program\n"
               "cat [file]...     --show the file content\n"
	       "check_fat         --check the vfat FAT table\n"
               "ls  [file|dir]... --list the file or diectory\n"
               "dir [file|dir}... --the same with 'ls'\n"
               "block [file|dir]. --show the file block address\n"
               "sector [file|dir] --show the file sector address\n"
               "stat [file|dir].. --show the detail information\n");
        
}
static int parse_shell(char *shell, char *argv[])
{
        int argc = 0;
        char *p = NULL;

        while(*shell == ' ')
                shell++;
        if (!*shell)
                goto out;
        while((p = strchr(shell, ' '))) {
                *p++ = 0;
                while(*p == ' ')
                        p++;
                argv[argc++] = shell;
                shell = p;
        }
        if (!*shell)
                goto out;
        argv[argc++] = shell;
out:
        return argc;
}

static inline int is_cmd(char *cmd, char *input)
{
        return !strcmp(cmd, input);
}


#define MAX_ARGC 255

int fstk_shell(const char *prompt, struct fstk *fs)
{
	int argc = 0;
	char **argv = malloc(MAX_ARGC * sizeof(char *));
	char *line;
	char *cmd;


         /* the shell part */
        printf("\tfstk shell, V%s (Copyright) Liu Aleaxander\n"
               "\ttype 'quit' or 'q' to quit, 'help' for more information\n",
	       VERSION_STR);
   
	while (!done) {
		line = readline(prompt);

		if (!line)
			break;
		add_history(line);

		argc = parse_shell(line, argv);
		cmd = argv[0];

                if (!argc) {
                        continue;
                } else if (is_cmd(cmd, "quit") || is_cmd(cmd, "q")) {
                        done = 1;
			break;
                } else if (is_cmd(cmd, "print")) {
                        if (is_cmd(argv[1], "fs"))
                                fs->ops->print_fs_info(fs);
                } else if (is_cmd(cmd, "help")) {
                        fstk_shell_help();
                } else if (is_cmd(cmd, "fs")) {
                        fstk_unmount(fs);                        
                        return MOUNT_AGAIN;
                } else if (is_cmd(cmd, "cat")) {
                        if (argc > 1)
                                fstk_cat(fs, argc, argv);
                        else
                                printf("USAGE: cat file\n");
                } else if (is_cmd(cmd, "ls")) {
                        fstk_ls(fs, argc, argv);
                } else if (is_cmd(cmd, "cd")) {
                        fstk_cd(fs, argv[1]);
		} else if (is_cmd(cmd, "check_fat")) {
			fstk_check_fat(fs);
                } else if (is_cmd(cmd, "block")) {
                        if (*argv[1])
                                fstk_block(fs, argc, argv);
                        else
                                printf("USAGE: block file | dir\n");
                } else if (is_cmd(cmd, "sector")) {
                        if (argc > 1)
                                fstk_sector(fs, argc, argv);
                        else
                                printf("USAGE: sector file | dir\n");
                } else if (is_cmd(cmd, "stat")) {
                        if (argc > 1)
                                fstk_stat(fs, argc, argv);
                        else
                                printf("USAGE: stat file | dir\n");
                } else
                        printf("Unknow command! type 'help' for more info.\n");
	}

	return 0;
}

