#ifndef FSTK_MALLOC_H
#define FSTK_MALLOC_H

/* The memory managemant structure */
struct mem_struct {
        struct mem_struct *prev;
        int size;
        int free;
};

void fstk_init_mem(void);
void *fstk_malloc(int);
void fstk_free(void *);
void check_mem(void);

static inline void malloc_error(char *obj)
{
        printf("malloc error: can't allocate memory for %s\n", obj);
        check_mem();
}


#endif /* fstk_malloc.h */
