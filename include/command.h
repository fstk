#ifndef COMMAND_H
#define COMMAND_H


/* shell.c */
int fstk_shell(const char *, struct fstk *fs);

/* cat.c */
void fstk_cat(struct fstk *, int, char **);

/* ls.c */
void fstk_ls(struct fstk *, int , char **);

/* cd.c */
void fstk_cd(struct fstk *, char *);

/* block.c */
void fstk_block(struct fstk*, int, char **);
void fstk_sector(struct fstk *, int , char **);

/* stat.c */
void fstk_stat(struct fstk *, int, char **);

/* check_fat.c */
void fstk_check_fat(struct fstk *);

#endif /* command.h */
