#ifndef LIB_H
#define LIB_H

/* 
 * The standard lib hearder of fstk
 */


/* lib/diskio.c */
extern uint32_t disk_read(struct fstk *, void *, uint32_t,
                          uint32_t);
extern uint32_t disk_write(struct fstk *, void *, uint32_t,
                           uint32_t);
extern uint32_t block_read(struct fstk *, void *, uint32_t,
                           uint32_t);
extern uint32_t block_write(struct fstk *, void *, uint32_t,
                            uint32_t);

/* lib/hexdump.c */
extern void hexdump(void *, int, int);

/* lib/utils.c */
extern inline int bsr(uint32_t);
extern void *free_alloc_mem(void *, void *, int);
extern int fstk_strccpy(char *, const char *);
extern char *fstk_strdup(const char *);
extern void tolower_str(char *);


static inline void print_time(time_t * time)
{
        struct tm *tm = localtime(time);
        
        printf("%02d-%02d-%4d %02d:%02d\n", tm->tm_mday, tm->tm_mon, 
                1900 + tm->tm_year, tm->tm_hour, tm->tm_min);
}
        
        

#endif /* lib.h */
