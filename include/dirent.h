#ifndef DIRENT_H
#define DIRENT_H

#include <stdint.h>

struct dirent {
        uint32_t d_ino;
        uint32_t d_off;
        uint16_t d_reclen;
        uint16_t d_type;
        char d_name[256];
};


typedef struct {
        struct file *dd_dir;
} DIR;

extern DIR *this_dir;

DIR * fstk_opendir(struct fstk *, char *);
struct dirent * fstk_readdir(DIR *);
void fstk_closedir(DIR *);

#endif /* dirent.h */
