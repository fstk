#ifndef FS_H
#define FS_H

#include "fstk.h"

enum fs_type {EXT2};


/* 
 * fs ops strucures
 */
extern struct fs_ops ext2_fs_ops;
extern struct fs_ops vfat_fs_ops;
extern struct fs_ops iso_fs_ops;

/*
 * functions 
 */
char *guess_fs(char *);
extern int ext2_guess_fs(struct fstk *);
extern int vfat_guess_fs(struct fstk *);

int fstk_mount(struct fstk *, char *);
void fstk_unmount(struct fstk *);
struct fstk * init_fs(char *);        


#endif /* fs.h */
