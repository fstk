#ifndef FSTK_FILE_H
#define FSTK_FILE_H

struct file {
        struct fstk *fs;      /* which fs we belong to */
        struct inode *inode;  /* the file-specific information */
        uint32_t offset;      /* for next read */
};

struct file *fstk_open(struct fstk *, const char *);
int fstk_read(struct file *, void *, uint32_t);
int fstk_lseek(struct file *, int, int);
void fstk_close(struct file *);


static inline char * file_type_str(struct inode *inode)
{
        if (inode->mode == DIR_TYPE)
                return "DIR";
        else if (inode->mode == FILE_TYPE)
                return "FILE";
        else if (inode->mode == SYMLINK_TYPE)
                return "SYMLINK";
        else
                return "Unknow";
}

#endif /* file.h */
