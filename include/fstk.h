#ifndef FSTK_H
#define FSTK_H

#include <stdint.h>
#include <time.h>


#define MOUNT_AGAIN 1

typedef uint64_t sector_t;
typedef uint64_t block_t;

/* 
 * The fstk structure 
 */
struct fstk {
        int fd;                  /* returned by open function */
        char fs_name[8];         /* the file system name */
        struct fs_ops *ops;      /* the fs-specific operations */
        struct inode *mount;     /* mount point */
        void *fs_info;           /* the fs-specific information */
        int sector_shift;
        int block_shift;
        uint32_t base;           /* where(as sector) to start as block */
};

struct file;

/* the fs operation structure */
struct fs_ops {
        int (*read) (struct file *, void *, int);
        int (*close) (struct file *);
        void (*stat) (struct file *);
        
        int (*mount) (struct fstk *);
        void (*unmount) (struct fstk *);

        /* show where the file live in in the disk (in sectors) */
        int  (*get_blocks) (struct file *, uint32_t **);
        /* get the file information */
        void (*get_file_info) (struct file *);
        /* print fs-specific file information */
        void (*print_file_info) (struct file *);
        void (*print_fs_info) (struct fstk *);

        struct inode  * (*iget_root)(void);
        struct inode  * (*iget)(char *, struct inode *);
        struct inode  * (*iget_by_inr)(uint32_t);
        struct dirent * (*readdir)(struct file *);

        char * (*follow_symlink)(struct inode *, const char *);
};


/*
 * Here we just care about _file_ and _dir_ type, so the value 
 * of _mode_ filed in _inode_ structure is ONLY FILE or DIR.
 */
enum inode_mode {FILE_TYPE, DIR_TYPE, SYMLINK_TYPE};


/* 
 * The inode structure, including the detail file information 
 */
struct inode {
        int          mode;   /* FILE or DIR */
        uint32_t     size;
        uint32_t     ino;    /* Inode number */
        time_t       atime;  /* Access time */
        time_t       mtime;  /* Modify time */
        time_t       ctime;  /* Create time */
        time_t       dtime;  /* Delete time */
        int          blocks; /* How many blocks the file take */
        uint32_t *   data;   /* The block address array where the file stores */
        uint32_t     flags;
        int          blkbits;
        int          blksize;
        uint32_t     file_acl;
};


extern void set_current_dentry(struct fstk *);
extern void free_current_dentry(void);
extern void fstk_free(void *);

static inline void free_inode(struct inode * inode)
{
        if (inode) {
                if (inode->data)
                        fstk_free(inode->data);
                fstk_free(inode);
        }
}
    
#define SECTOR_SIZE(fs)   (1 << fs->sector_shift)
#define BLOCK_SIZE(fs)    (1 << fs->block_shift)


#endif /* fstk.h */
