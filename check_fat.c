/*
 * The FAT entry check for VFAT fs(as you may know from the command name)
 *
 * Copyright (C) 2010 Liu Aleaxander -- All rights reserved. This file
 * can be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <string.h>
#include "fstk.h"

void fstk_check_fat(struct fstk *fs)
{
	if (!strstr(fs->fs_name, "fat")) {
		printf("ERROR: It seems that you are running this command"
			"under a non-vfat file system\n");
		return;
	}
	
	vfat_check_fat(fs);
}
