/*
 * The main file in fstk project
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * may be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "version.h"
#include "fstk.h"
#include "fs.h"
#include "file.h"
#include "fstklib.h"
#include "fstk_malloc.h"
#include "cache.h"
#include "command.h"

        
static void usage(void)
{
        printf("USAGE: fstk -t fs_type fs.img\n");
        printf("OR     fstk fs.img, fstk will recognise it correct for you\n");
        exit(0);
}

/*
 * Initialize the envrionments used by fstk, like this_dentry...
 */
void init_fstk_env(struct fstk *fs)
{
        set_current_dentry(fs);
}

void free_fstk_env(void)
{
        free_current_dentry();
}

int main(int argc, char *argv[])
{

        struct fstk *fs;
        char *fs_type;
        char *fs_img;
	int res;
        
mount_again:
        fs_img = argv[1];  /* guess it first */
        if (argc == 2) 
                fs_type = guess_fs(fs_img);
        else if (argc == 4 && !strcmp(argv[1], "-t")) {
                fs_type = argv[2];
                fs_img = argv[3];
        } else
                usage();
        
        if (fs_type == NULL) 
		usage();

        /* Init fstk memory fisrt */
        fstk_init_mem();
        fs = init_fs(fs_type);
        if (!fs) {
                printf("Please input the correct file system type.\n");
                printf("we currently suport EXT2/3/4 file systems\n\n");
                usage();
        }
        if (fstk_mount(fs, fs_img)) {
                printf("mount error, "
                       "maybe you haven't input the correct fs type\n");
                usage();
        }
        /* Init the cache */
        init_cache(fs);
        
        /* Init the Environment used by fstk */
        init_fstk_env(fs);
        
	/* we all count on it */
	res = fstk_shell("fstk:> ", fs);
	if (res == MOUNT_AGAIN)
		goto mount_again;
        
        free_fstk_env();
        fstk_unmount(fs);
        check_mem();       /* for debug use to check how much memory we lose */
        return 0;
}
                
