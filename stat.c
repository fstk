/*
 * stat command. Note: it's not the stat function in Linux.
 *
 * Copyright (C) 2009 Liu Aleaxander -- All rights reserved. This file
 * can be redistributed under the terms of the GNU Public License.
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "fstk.h"
#include "file.h"
#include "dirent.h"
#include "fstklib.h"

/*
 * print the detail information of the specified file.
 */
void fstk_stat(struct fstk *fs, int argc, char *argv[])
{
        struct file *file;
        struct inode *inode;
        int i = 1;

        for (; i < argc; i++) {
                if (!(file = fstk_open(fs, argv[i]))) {
                        printf("stat: can't access %s: "
                               "No such file or directory\n", argv[i]);
                        continue;
                }
                inode = file->inode;
                printf("Inode number: %-10u\n", inode->ino);
                printf("File name   : %s\t    type: %s    size: %-10u\n", 
                       argv[i], file_type_str(inode), inode->size);
                printf("Create time: ");
                print_time(&inode->ctime);
                printf("Modify time: ");
                print_time(&inode->mtime);
                printf("Access time: ");
                print_time(&inode->atime);
                if (inode->dtime) {
                        printf("Delete time: ");
                        print_time(&inode->dtime);
                }
                fs->ops->stat(file);

                fstk_close(file);
                printf("\n");
        }
}
